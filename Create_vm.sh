#!/bin/bash
# https://gemmei.ftp.acc.umu.se/debian-cd/current/amd64/iso-cd/debian-12.5.0-amd64-netinst.iso

VBoxManage="/mnt/c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe"

VBoxManage createvm --name Debian_IDU_0 --ostype Debian_64 --register
VBoxManage modifyvm Debian_IDU_0 --cpus 1 --memory 2048 --vram 16
VBoxManage modifyvm Debian_IDU_0 --nic1 bridged --bridgeadapter1 'Intel(R) Wireless-AC 9560 160MHz'
VBoxManage createhd --filename ./Debian_IDU_0.vdi --size 5120 --variant Standard
#VBoxManage createhd --filename /mnt/c/Users/$USER/VirtualBox\ VMs/Debian_IDU_0/Debian_IDU_0.vdi --size 5120
VBoxManage storagectl Debian_IDU_0 --name "SATA Controller" --add sata --bootable on
VBoxManage storageattach Debian_IDU_0 --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium ./Debian_IDU_0.vdi
#VBoxManage storageattach Debian_IDU_0 --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium /mnt/c/Users/$USER/VirtualBox\ VMs/Debian_IDU_0/Debian_IDU_0.vdi
VBoxManage storagectl Debian_IDU_0 --name "IDE Controller" --add ide
VBoxManage storageattach Debian_IDU_0 --storagectl "IDE Controller" --port 0  --device 0 --type dvddrive --medium "c:\Users\\$USER\Downloads\debian-12.5.0-amd64-netinst.iso"
# VBoxManage storageattach Debian_IDU_0 --storagectl "IDE Controller" --port 0  --device 0 --type dvddrive --medium /mnt/c/Users/$USER/Downloads/debian-12.5.0-amd64-netinst.iso
VBoxManage startvm Debian_IDU_0
